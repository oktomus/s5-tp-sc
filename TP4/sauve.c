/*!
* \file : sauve.c
* \brief Programm de sauvegarde d'arborescence distribué
* \author : Kevin Masson
* \data : 23 Novembre 2016
*/

/*!
* Question 1
* 
*     SYNCHRONISATION
*
* On a ici deux tampons qui sont confrontés au problème du producteur/consommateur. 
*
* Buffer des fichiers:
*     Pour la synchronisation de ce buffer, un mutex est utilisé pour l'accés critique en lecture et modification du buffer. On a aussi une 
* condition sous laquelle le buffer ne doit pas etre vide pour que l'on puisse lire dans le buffer. Pour cela, j'utilise une condition POSIX
* qui est bloquante à la lecture si le buffer est vide, est qui est debloqué à l'écriture. De plus, le buffer ayant une taille finie,
* l'écriture n'est possible que si il y a de la place. Pour cela, j'utlise une condition qui est bloquante si il n'y a plus de place, et qui
* est débloqué à la lecture.
*
* Buffer des repertoire:
*    La synchronisation de ce buffer est effectué qu'uniquement avec un mutex et une condition. Le buffer étant infini, aucune condition n'est 
* nécessaire pour l'écriture. Le mutex permet l'accés en section critique pour modifier ou lire le buffer. La condition permet de mettre le 
* thread en attente si celui essaye de lire et qu'il n'y a rien à lire. C'est une écriture dans le buffer qui débloquera la condition.
*
*    ARRET DES THREADS
*
*    Pour pouvoir arrêter les threads, plusieurs conditions sont nécessaires. Pour ce qui est des scanner, il doivent s'arrêter lorsqu'il n'y a plus
* de travail, c.à.d, lorsque le buffer des repertoires est vide et qu'aucun autre scanner est en train de lire un repertoire. Si ces deux conditions sont réunnies,
* alors les scanners peuvent s'arreter.
*    Pour les analyser, ceux-ci ne peuvent pas s'arreter si le buffer des fichiers est vide et qu'aucun autre analyser ne travail. Car même si c'est le cas,
* un scanner peut etre en train de lire un repertoire et ajouter des fichiers dans le buffer alors qu'aucun analyser ne travaille. Les analyer dépendent totalement
* des scanner car ce sont eux qui donne du travail aux analyser, on peut donc arrêter ceci lorsque le buffer des fichiers est vide, et que tout les scanner aient fini.
*
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <stddef.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <time.h>
#include <libgen.h>
#include <sys/stat.h>

int errno;
clock_t start; // Pour le debug

//////////////////////////////////////////////////////
// ---UTILS FONCTIONS
/////////////////////////////////////////////////////

/*!
* \biref Test if a given directory exists
*/
int dir_exists(const char * path){
	DIR* dir = opendir(path);
	if (dir)
	{	    
	    closedir(dir);
	    return 1;
	}
	else if (errno == ENOENT)
	{
	    return 0;
	}
	else
	{
	    return 0;
	}
}

/*!
* \brief Check if a file has changed from the last backup
* Return > 0 if yes
* < 0 otherwise
*/
int file_has_changed(const char * original, const char * backup){
	struct stat stat_orig;
	struct stat stat_bckp;

	if (stat(original, &stat_orig) == -1) {
		return 1;
	}
	if (stat(backup, &stat_bckp) == -1) {
		return 1;
	}

	if(stat_bckp.st_size != stat_orig.st_size) return 1;

	if(stat_bckp.st_mtim.tv_nsec == stat_orig.st_mtim.tv_nsec) return 1;

	if(stat_bckp.st_mode != stat_orig.st_mode) return 1;

	return 0;



}

/*!
* \brief Retourne un chemin de sauvegarde en se basant sur le chemin de base
*/
char *  path_for_save(char * original_path, const char * save_dir, const char * base_dir){
	if(strcmp(original_path, base_dir) == 0){
		char * save_path = (char *) malloc(sizeof(char) * strlen(save_dir));
		sprintf(save_path, "%s", save_dir);
		return save_path;
	}else{
		char slash = '/';
		char * save_path = (char *) malloc(sizeof(char) * (strlen(original_path) - strlen(base_dir) + 1 + strlen(save_dir)));
		char * ptr_write = save_path;

		strncpy(ptr_write, save_dir, strlen(save_dir));
		ptr_write += strlen(save_dir);
		
		strncpy(ptr_write, &slash, 1);
		ptr_write += 1;
		
		char * ptr_file = original_path + strlen(base_dir) + 1;
		strncpy(ptr_write, ptr_file, strlen(original_path) - strlen(base_dir));
		
		return save_path;

	}
}

/*!
* \brief Return the time in seconds since the execution of the program
*/
double exec_time(){
	clock_t now = clock();
	return (double)(now - start) / CLOCKS_PER_SEC;
}

/*!
* \biref Return the concat version of 2 string
*/
char* concat(const char *s1, const char *s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1);//+1 for the zero-terminator
    //in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

//////////////////////////////////////////////////////
// --- OPTIONS
/////////////////////////////////////////////////////

const char * o_source = NULL, * o_precedent = NULL, * o_destination = NULL;	
int o_debug = 0, o_save_debug = 0, o_scanner = 1, o_analyser = 1,  o_buffer_size = 10;

//////////////////////////////////////////////////////
// --- PATH STRUCTURE
/////////////////////////////////////////////////////

struct path{
	size_t length;
	char value[0];
};

struct linked_path{
	size_t length;
	struct linked_path * suivant;
	char value[0];
};

//////////////////////////////////////////////////////
// --- BUFFER DES FICHIERS STRUCTURE
/////////////////////////////////////////////////////

struct buffer_fichiers{
	size_t indice_ecriture; // Indice d'écriture
	size_t indice_lecture; // Indice de lecture
	size_t size; // Taille max du buffer
	int scanner_end; // Boolean pour savoir si les scanner sont fini
	pthread_cond_t cond_ecriture; // Condition pour l'écriture, bloquante si il n'y a pas de place
	pthread_cond_t cond_lecture; // Condition pour la lecture, bloquante si buffer vide	
	pthread_mutex_t mut_critique; // Mutex pour les sections critiques
	int quantite; // Nombre de fichiers dans le buffer
	struct path *fichiers[0];
};

struct buffer_fichiers *bf_f;

//////////////////////////////////////////////////////
// --- BUFFER DES REPERTORES STRUCTURE
/////////////////////////////////////////////////////

struct buffer_repertoires{
	pthread_cond_t cond_conso; // Condition pour les consommations, bloquant si il n'y a rien a lire
	pthread_mutex_t mut_critique; // Mutex pour les sections critiques
	int worker; // Nombre de scannage en cours
	struct linked_path *premier;
	struct linked_path *dernier;	
};

struct buffer_repertoires *bf_r;

//////////////////////////////////////////////////////
// --- FONCTIONS FICHIERS
/////////////////////////////////////////////////////


/*!
* \brief Initilise le buffer des fichiers
*/
int init_buffer_fichiers(const size_t taille){
	if(o_debug) printf("Initialisation du buffer de fichier avec une taille de %zu\n", taille);
	bf_f = (struct buffer_fichiers *) malloc(sizeof(struct buffer_fichiers) + sizeof(struct fichier *) * taille);
	if(bf_f == NULL) return -1;
	bf_f->indice_ecriture = 0;
	bf_f->quantite = 0;
	bf_f->indice_lecture = 0;
	bf_f->size = taille;
	bf_f->scanner_end = 0;
	if(pthread_cond_init(&bf_f->cond_lecture, NULL) != 0){
		perror("Can't init pthread cond for file buffer");
		exit(EXIT_FAILURE);
	} 
	if(pthread_cond_init(&bf_f->cond_ecriture, NULL) != 0){
		perror("Can't init pthread cond for file buffer");
		exit(EXIT_FAILURE);
	} 
	pthread_mutex_t tmp = PTHREAD_MUTEX_INITIALIZER; 
	bf_f->mut_critique = tmp;
	return 1;
}

/*!
* \brief Libere le buffer des fichiers
*/
int clear_buffer_fichiers(){
	if(o_debug) printf("Liberation du buffer des fichiers\n" );
	if(bf_f == NULL) return -1;
	if(o_debug) printf("-- %zu fichier(s) \n", bf_f->size);

	// Pas besoin de faire de free ici, les free sont fait au moment de la recuperation dans les buffer

	pthread_cond_destroy(&bf_f->cond_lecture);
	pthread_cond_destroy(&bf_f->cond_ecriture);
	free(bf_f);
	return 1;
}

/*!
* \biref Ajoute un fichier au buffer de fichiers
*/
int buf_ajouter_fichier(const char * path){


	pthread_mutex_lock(& bf_f->mut_critique); // Section critique	


	while(bf_f->quantite >= bf_f->size){ // Si il n'y a pas de place, on attend.
		pthread_cond_wait(&bf_f->cond_ecriture, &bf_f->mut_critique);
	}

	if(o_debug) printf("(%f) Ajout du fichier %s au buffer à l'indice %zu\n", exec_time(), path, bf_f->indice_ecriture);

	// Obliger de creer un pointeur de ref pour pouvoir modifier le buffer globalement
	struct path **f = &(bf_f->fichiers[bf_f->indice_ecriture]);
	*f = (struct path *) malloc(sizeof(struct path) + strlen(path) * sizeof(char));
	if(*f == NULL){
		printf("Ajout : Impossible d'accéder au fichier pour ajout\n");
		exit(EXIT_FAILURE);
	}
	(*f)->length = strlen(path);
	strcpy((*f)->value, path);
	bf_f->quantite += 1;
	bf_f->indice_ecriture  = (bf_f->indice_ecriture + 1) % (bf_f->size);

	pthread_cond_signal(&bf_f->cond_lecture); // Signaler aux analyser lecteurs que le buffer se remplit

	pthread_mutex_unlock(& bf_f->mut_critique);	// Fin section critique
	if(o_debug) printf("(%f) Fin d'ajout du fichier %s\n", exec_time(), (*f)->value);


	return 1;
}

/*!
* \biref Recupere un fichier du buffer de fichiers
*/
int buf_recuperer_fichier(struct path ** output){
	pthread_mutex_lock(& bf_f->mut_critique); // Section critique

	while(bf_f->quantite == 0 && bf_f->scanner_end == 0){ // Si il n'y a rien a lire et que ce n'est pas la fin, on attend
		// La fin est marqué par la fin des scanner 
		pthread_cond_wait(&bf_f->cond_lecture, &bf_f->mut_critique); 
	}

	if(o_debug) printf("(%f) Recupération d'un fichier à l'indice %zu (stop:%d) (quantite:%d)\n-", exec_time(), bf_f->indice_lecture, bf_f->scanner_end, bf_f->quantite);

	if(bf_f->quantite <= 0 && bf_f->scanner_end == 1){ // Si il n'y a rien a lire et que aucune scanner ne process, on quitte
		pthread_mutex_unlock(&bf_f->mut_critique); // Section critique
		return 0;
	}

	// On copie la donne pour pouvoir la liberer du buffer
	struct path **modele = &(bf_f->fichiers[bf_f->indice_lecture]);
	int size = sizeof(struct path) + (*modele)->length * sizeof(char); 
	*output = (struct path *) malloc(size);
	(*output)->length = (*modele)->length;
	strcpy((*output)->value, (*modele)->value);
	if(*output == NULL){
		printf("Recuperation : Impossible de recuperer le fichier\n");
		exit(EXIT_FAILURE);
	}
	free(*modele);
	bf_f->indice_lecture = (bf_f->indice_lecture + 1) % (bf_f->size); 
	bf_f->quantite -= 1;

	pthread_cond_signal(&bf_f->cond_ecriture);

	pthread_mutex_unlock(& bf_f->mut_critique); // Section critique

	if(o_debug) printf("(%f) Fin de récuperation du fichier %s\n", exec_time(), (*output)->value);
	return 1;
}

/*!
* \brief Avertis le buffer de fichiers que plus rien ne sera ajouté
* Permet d'arrêter les analyser
*/
int buf_fichier_notify_end(){
	pthread_mutex_lock(& bf_f->mut_critique); // Section critique

	// On notifie les analyser que les scanner ont fini
	bf_f->scanner_end = 1;
	// On debloque tout les lecteurs
	// On aurait pu aussi ne debloquer que le dernier lecteur si il reste des valeurs dans le buffer
	// Mais il aurait fallu gerer ca en profondeur pour ceux qui continue à attendre
	pthread_cond_broadcast(&bf_f->cond_lecture);

	pthread_mutex_unlock(& bf_f->mut_critique); // Section critique

}

//////////////////////////////////////////////////////
// --- FONCTIONS REPERTORES
/////////////////////////////////////////////////////

/*!
* \brief Initialise le buffer des repertoires
*/
int init_buffer_repertoires(){
	bf_r = (struct buffer_repertoires *) malloc(sizeof(struct buffer_repertoires));
	if(bf_r == NULL) return -1;
	pthread_mutex_t tmp = PTHREAD_MUTEX_INITIALIZER; 
	bf_r->mut_critique = tmp;
	bf_r->worker = 0;
	if(pthread_cond_init(&bf_r->cond_conso, NULL) != 0){
		perror("Can't init pthread cond for rep buffer");
		exit(EXIT_FAILURE);
	} 
	bf_r->premier = NULL;
	bf_r->dernier = NULL;
	return 0;
}

/*!
* \brief Libere le buffer des repertoires
*/
int clear_buffer_repertoires(){
	if(o_debug) printf("Liberation du buffer des repertoires\n");
	if(bf_r == NULL) return -1;
	// Pas de free necessaire ici
	pthread_cond_destroy(&bf_r->cond_conso);
	free(bf_r);
	return 0;
}


/*!
* \brief Ajoute un repertoire au buffer des repertoires
*/
int buf_ajouter_repertoire(const char * path){
	pthread_mutex_lock(& bf_r->mut_critique); // Debut section critique 
	if(o_debug) printf("Ajout du rep %s au buffer\n", path);
	// creation du chemin dynamique
	struct linked_path *rep;
	rep = (struct linked_path *) malloc(sizeof(struct linked_path) + strlen(path) * sizeof(char));
	if(rep == NULL){
		printf("Ajout : Allocation repertoire impossible \n");
		exit(EXIT_FAILURE);
	}
	rep->suivant = NULL;
	rep->length = strlen(path);
	strcpy(rep->value, path);

	// Pointeur de pointeur pour pouvoir modifier le buffer globalement sans soucis
	struct linked_path **end = &bf_r->dernier;
	struct linked_path **start = &bf_r->premier;
	if(*start == NULL){
		*start = rep;
		*end = rep;
	}else{
		(*end)->suivant = rep;
		(*end) = rep;
	}
	pthread_cond_signal(&bf_r->cond_conso); // On signal les consommateurs qu'il y a de quoi lire
	pthread_mutex_unlock(& bf_r->mut_critique); // Fin section critique 

	return 1;
}

/*!
* \brief Recupere un repertoire du buffer des repertoires
*
* Après chaque appel à cette fonction, il faut appeler buf_scanner_endwork pour s'assurer que les threads s'arreteront
*/
int buf_recuperer_repertoire(struct path ** output){
	pthread_mutex_lock(& bf_r->mut_critique); // Debut section critique 

	while((bf_r->premier == NULL || bf_r->dernier == NULL) && bf_r->worker > 0){ // Si il n'y a rien a lire et que des scannage sont en cours
		pthread_cond_wait(&bf_r->cond_conso, &bf_r->mut_critique); // On attend
	}
	
	if(o_debug) printf("Scanner : Recuperation d'un reperetoire (worker:%d)\n", bf_r->worker);

	if((bf_r->premier == NULL || bf_r->dernier == NULL) && bf_r->worker <= 0){ // Si il n'y a rien a lire et que aucune scanner ne process, on quitte
		pthread_mutex_unlock(&bf_r->mut_critique); // Debloque Section critique
		return 0;
	}

	bf_r->worker += 1; // On notifie notre arrivée

	// Copie les données
	struct linked_path ** modele = &(bf_r->premier);
	*output = (struct path *) malloc(sizeof(struct path) + (*modele)->length * sizeof(char));
	(*output)->length = (*modele)->length;
	strcpy((*output)->value, (*modele)->value);

	// Modification de la chaine
	struct linked_path **end = &bf_r->dernier;
	struct linked_path **start = &bf_r->premier;
	if(*end == *start){
		free(*end);
		*end = NULL;
		*start = NULL;
	}else{
		struct linked_path **next = &(*start)->suivant;
		free(*start);
		*start = *next;		
	}

	if(o_debug) printf("Scanner : FIN Recuperation d'un reperetoire (worker:%d)\n", bf_r->worker);


	pthread_mutex_unlock(& bf_r->mut_critique); // Fin section critique 
	return 1;
}

/*!
* \brief Fonction qu'un scanner doit appeller pour marquer la fin de son travail
*/
int buf_scanner_endwork(){
	pthread_mutex_lock(& bf_r->mut_critique); // Section critique

	bf_r->worker -= 1;
	if(bf_r->worker <= 0){ // Si plus personne ne travail
		if(bf_r->premier == NULL || bf_r->dernier == NULL) buf_fichier_notify_end(); // Si en plus le buffer est vide, on annonce notre fin aux analyser
		pthread_cond_broadcast(&bf_r->cond_conso); // Notifie tous les scanner lecteur de se reveiller
	} 

	pthread_mutex_unlock(& bf_r->mut_critique); // Section critique
	return 1;
}


//////////////////////////////////////////////////////
// --- MAIN
/////////////////////////////////////////////////////

int main(int argc, const char * argv[]){

	start = clock();

	// --------------------------------------------------------------------- Parsing des arguments
	int i;
	for(i = 1; i < argc; ++i){
		//printf("Parsing %s\n", argv[i]);
		if(strcmp(argv[i], "-n")==0){			
			o_save_debug = 1;
		}else if(strcmp(argv[i], "-n2")==0){			
			o_debug = 1;
			o_save_debug = 1;
		}else if(strcmp(argv[i], "-s")==0){
			o_scanner = atoi(argv[i+1]);
			++i;			
		}else if(strcmp(argv[i], "-a")==0){
			o_analyser = atoi(argv[i+1]);
			++i;
		}else if(strcmp(argv[i], "-f")==0){
			o_buffer_size = atoi(argv[i+1]);
			++i;
		}else{
			if(o_source == '\0'){
				o_source = argv[i];
				//o_source = argv[i];
			}else if(i < argc - 1 && o_precedent == '\0'){
				o_precedent = argv[i];
				//o_precedent = argv[i];
			}else if(o_destination == '\0'){
				o_destination = argv[i];
				//o_destination = argv[i];				
			}
		}
	}

	if(o_debug) printf("Source (%s) Dest (%s) Prec(%s)\n", o_source, o_destination, o_precedent);

	if(o_source == NULL || o_destination == NULL){
		printf("FAIL: %s  [-n[2]] [-s n] [-a n] [-f n] source [précédent] destination\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	// ---------------------------------------------------------------------  Test arg dest et source et prec
	if(o_destination == o_source){
		printf("FAIL: urce and the destination can't be the same\n");
		exit(EXIT_FAILURE);
	}
	
	// Test if destination exists
	if(dir_exists(o_destination)){
		printf("FAIL: directory %s already exists\n", o_destination);
		exit(EXIT_FAILURE);
	}
	// Test if source exists
	if(dir_exists(o_source) != 1){
		printf("FAIL: directory %s doesn't exists\n", o_source);
		exit(EXIT_FAILURE);
	}
	
	// Test if precedent exists
	if(o_precedent != NULL){
		if(dir_exists(o_precedent) != 1){
			printf("FAIL: directory %s doesn't exists\n", o_precedent);
			exit(EXIT_FAILURE);
		}
	}

	// ---------------------------------------------------------------------  Debut du programme

	// Initilisation u buffer fichiers
	if(init_buffer_fichiers(o_buffer_size) < 0){
		printf("FAIL: init files buffer\n");
		exit(EXIT_FAILURE);
	}

	if(init_buffer_repertoires() <0){
		printf("FAIL: init folders buffer\n");
		exit(EXIT_FAILURE);
	}


	if(buf_ajouter_repertoire(o_source) < 0){
		printf("FAIL: add source folder to buffer\n");
		exit(EXIT_FAILURE);
	}

	// Creation des variables pour thread

	pthread_t th_scanner[o_scanner], th_analyser[o_analyser];

	//////////////////////////////////////////////////////
	// --- FONCTION DES THREADS
	/////////////////////////////////////////////////////

	void * scanner(void *arg){
			if(o_debug) printf("Lancement d'un scanner\n");

			int feedback;
			struct path * current = NULL; // Repertoire en train d'être scanné'
			int nb_file;
			char *filename = NULL;
			char *save_dir = NULL;
			char * exec_command = NULL;
			struct dirent ** files_list = NULL;
			int file_type;

			// Pour chaque fichier lu
			for(;;){
				free(files_list);
				free(current);
				free(save_dir);
				free(exec_command);
				files_list = NULL;
				current = NULL;
				save_dir = NULL;
				exec_command = NULL;
				feedback = buf_recuperer_repertoire(&current);
				if(feedback < 0){
					printf("FAIL: Recuperation repertoire\n");
					exit(EXIT_FAILURE);
				}else if(feedback == 0){
					if(o_debug){
						printf("== Fin Scanner\n");
					}
					break; // Plus de travail

				}  
				// Analyse du repertoire
				if(o_debug) printf("Scanner : Lecture du repertoire %s (len:%zu)\n", current->value, strlen(current->value));



				save_dir = path_for_save(current->value, o_destination, o_source);

				exec_command = concat(  // Commande à executer
					"mkdir",
					concat(
						" ",
						save_dir
					)
				);

				if(o_save_debug){
					printf("%s\n", exec_command);
				}else{
					int status = system(exec_command); // Execution de la commande
					if(status != 0){
						perror(concat("Failed to execute command ", exec_command));
						exit(EXIT_FAILURE);
					}
				}

				// Lecture du dossier
				nb_file = scandir(current->value, &files_list, NULL, alphasort);

				int i;
				for (int i = 0; i < nb_file; i++)
				{
					// Tout sauf . et ..
					if(strcmp(files_list[i]->d_name, ".") != 0 && strcmp(files_list[i]->d_name, "..") != 0){ 
						file_type = files_list[i]->d_type;
						filename = NULL;
						filename = (char *) malloc(sizeof(char) * ( strlen(current->value) + 1 + strlen(files_list[i]->d_name) ));
						sprintf(filename, "%s/%s", current->value, files_list[i]->d_name);
						if(file_type == 8){  // C'est un fichier
							if(buf_ajouter_fichier(filename) < 0){
								printf("FAIL: add source folder to buffer\n");
								exit(EXIT_FAILURE);
							}
						}else if(file_type == 4){ // C'est un dossier
							if(buf_ajouter_repertoire(filename) < 0){
								printf("FAIL: add source folder to buffer\n");
								exit(EXIT_FAILURE);
							}
						}
						if(o_debug) printf("Scanner : Fichier %s scanné type(%d)\n", filename, files_list[i]->d_type);
						free(filename);
						filename = NULL;
					}
					free(files_list[i]);
				}

				buf_scanner_endwork(); // On notifie les autres scanner de la fin de notre travail
			}

			pthread_exit(arg);
	}



	void * analyser(void *arg){
			if(o_debug) printf("Lancement d'un analyser\n");

			int feedback;
			struct path * current = NULL;
			char *save_path = NULL;
			char * base_save = (char *) malloc(sizeof(char) * (strlen(o_source) + 1));
			char * exec_command = NULL;
			sprintf(base_save, "%s/", o_source);
			
			// Pour chaque fichier
			for(;;){
				free(current);
				free(save_path);
				free(exec_command);
				current = NULL;
				save_path = NULL;
				exec_command = NULL;
				feedback = buf_recuperer_fichier(&current);
				if(feedback < 0){
					printf("FAIL: Recuperation fichier\n");
					exit(EXIT_FAILURE);
				}else if(feedback == 0){ 
					if(o_debug) printf("== Fin Analyser\n");
					break; // Plus de travail
				}else{

					if(o_debug) printf("Analyser : Process du fichier %s len(%zu/%zu)\n", current->value, current->length, strlen(current->value) );

					save_path = path_for_save(current->value, o_destination, o_source);
					
					

					if(o_debug) printf("Analyser : Chemin de sauvegarde : %s\n", save_path);
					
					if(o_precedent == NULL || file_has_changed(current->value, path_for_save(current->value, o_precedent, o_source))){ // Si le fichier a changé

						exec_command = concat( // Commande
										concat(
											concat("cp ", current->value)
											, " ")
										,dirname(save_path)
									);
						if(o_save_debug){
							printf("%s\n", exec_command);  
						}else{
							int status = system(exec_command); // Execution de la commande
							if(status != 0){
								perror(concat("Failed to execute command ", exec_command));
								exit(EXIT_FAILURE);
							}
						}

					}else{ // Sinon on creer un hardlink

						exec_command = concat(
										concat(
											concat("ln ", path_for_save(current->value, o_precedent, o_source))
											, " ")
										,save_path
									);
						if(o_save_debug){
							printf("%s\n", exec_command);  // On copie
						}else{
							int status = system(exec_command);
							if(status != 0){
								perror(concat("Failed to execute command ", exec_command));
								exit(EXIT_FAILURE);
							}
						}

					}


				}

			}

			if(o_debug) printf("Fin d'un analyser\n");


			pthread_exit(arg);
	}

	// Creation des threads scanner
	for(i=0; i<o_scanner; i++){
		if((errno = pthread_create(&th_scanner[i], NULL, scanner, NULL)) != 0){
			perror("FAIL: anner create");
			exit(EXIT_FAILURE);
		};
	}

	if(o_debug) printf("Début de la sauvegarde\n");

	// Creation des threads analyser
	for(i=0; i<o_analyser; i++){
		if((errno = pthread_create(&th_analyser[i], NULL, analyser, NULL)) != 0){
			perror("FAIL: alyser create");
			exit(EXIT_FAILURE);
		};
	}

	// Attente des scanner
	for(i=0; i<o_scanner; i++){
		if((errno = pthread_join(th_scanner[i], NULL)) != 0){
			perror("Pthread scanner join");
		};
	}

	// Attente des analyser
	for(i=0; i<o_analyser; i++){
		if((errno = pthread_join(th_analyser[i], NULL)) != 0){
			perror("Pthread analyser join");
		};
	}

	if(clear_buffer_fichiers() < 0){
		printf("FAIL: clear files buffer\n");
		exit(EXIT_FAILURE);
	}

	if(clear_buffer_repertoires() <0){
		printf("FAIL: clear folders buffer\n");
		exit(EXIT_FAILURE);
	}

	if(o_debug) printf("Fin de la sauvegarde\n");


	return 0;

}
