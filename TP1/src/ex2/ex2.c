/**
* L3 Informatique
* SystemesConcurrents : 19/10/2016
* TP1 - Evercice 2
* Kevin Masson
*/

/**

	Au cours de la mise au point du programme précédent, vous avez certainement laissé des segments de mé-
	moire partagée actifs sans les avoir supprimés. Faites le point sur ces segments avec la commande ipcs et
	supprimez les segments inutiles avec la commande ipcrm.



*/
