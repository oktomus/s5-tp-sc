/**
* L3 Informatique
* SystemesConcurrents : 19/10/2016
* TP1 - Evercice 1
* Kevin Masson
*/

/**
		Écrivez un programme composé d’un processus père et de 4 processus fils se partageant un segment de mé-
		moire partagée de taille suffisante pour un compteur de type long int. Chaque processus fils incrémente un
		million de fois ce compteur. Une fois les processus fils terminés, affichez la valeur du compteur.

			shm partagé par les fils d'un proco (1 long int)
				- clé IPC_PRIVATE
				- shmget(..., IPC_CREATE|0600)
					+ shmat(...)
					avant les 4 fork
				- shmctl(..., IPC,RMID)
				apres les waits

		Que constatez-vous ?

		On atteind le int finale souhaité car les processus ne s'éxecute pas séquentiellement et la mémoire n'est pas parfaitement cohérente, lors d'une incrementation, la valeur peut avoir changé
		et lors de l'enregistrement, des données seront perdus
*/

#include <stdio.h> // print
#include <stdlib.h> // exit
#include <unistd.h> // fork
#include <sys/ipc.h> // ipc
#include <sys/shm.h> // shm
#include <sys/wait.h> // shm

int main(int argc, char **argv){

	pid_t pid;
	int shmid;
	long int * val;

	shmid = shmget(IPC_PRIVATE, sizeof(long int), IPC_CREAT|0600);
	val = shmat(shmid, NULL, 0);	
		
	int i;
	for(i=0; i<4;i++){
		pid = fork();

		if( pid == 0 ){ // PS FILS			
			int j;				
			for(j = 0; j< 1000000;j++){
				(*val)++;
				//printf("%ld\n", *val);
			}
			//printf("%ld\n", *val);
			exit(0);
		}
		else if(pid == -1){ // PS FAIL
			perror("Fail fork\n");
			exit(1);
		}

	}
	
	int status;	
	
	for(i=0; i<4; i++){
		wait(&status);	
	}

	shmctl(shmid, IPC_RMID, NULL);
	
	printf("Valeur finale %ld\n", *val);

	
	exit(0);
}
