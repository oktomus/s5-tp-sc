/**
* L3 Informatique
* SystemesConcurrents : 19/10/2016
* TP1 - Evercice 3
* Kevin Masson
*/

/**

	Pour résoudre le problème vu dans la première question, on propose d’utiliser un sémaphore binaire. Implé-
	mentez cette solution.

		1 Sempahore binaire/mutex // Sorte de valeur permettant de savoir si l'accès est possible
		
		Principe:
			1- Avant l'accès à la mem. paratagée
				-> LOCK, WAIT, P
			2- Après l'accès
				-> UNLOCK, POST, V
		Comment:	
			- IPC System V: sem... (ens. de sémaphores)
			- semget(IPC_PRIVATE, ...)
			- semctl(semid, 0, SETVAL, 1)
			- semop // +1 ou -1
*/


#include <stdio.h> // print
#include <stdlib.h> // exit
#include <unistd.h> // fork
#include <sys/ipc.h> // ipc
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>

int main(int argc, char **argv){

	pid_t pid;
	int shmid, semid;
	long int * val;
	struct sembuf op;
	op.sem_num = 0;
	op.sem_flg = 0;


	semid = semget(IPC_PRIVATE, 1, IPC_CREAT|0600);
	semctl(semid, 0, SETVAL, 1);
	shmid = shmget(IPC_PRIVATE, sizeof(long int), IPC_CREAT|0600);
	
	val = shmat(shmid, NULL, 0);	
	
	
		
	int i;
	for(i=0; i<4;i++){
		pid = fork();

		if( pid == 0 ){ // PS FILS			
			int j;				
			for(j = 0; j< 1000000;j++){
				op.sem_op = -1; // LOCK
				semop(semid, &op, 1);
				(*val)++;
				op.sem_op = +1; // UNLOCK
				semop(semid, &op, 1);
			}
			//printf("%ld\n", *val);
			exit(0);
		}
		else if(pid == -1){ // PS FAIL
			perror("Fail fork\n");
			exit(1);
		}

	}

	int status;	
	
	for(i=0; i<4; i++){
		wait(&status);	
	}

	shmctl(shmid, IPC_RMID, NULL);
	semctl(semid, 0, IPC_RMID, 1);
	
	printf("Valeur finale %ld\n", *val);

	
	exit(0);
}
