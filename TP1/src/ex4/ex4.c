/**
* L3 Informatique
* SystemesConcurrents : 19/10/2016
* TP1 - Evercice 4
* Kevin Masson
*/

/**
	On désire maintenant faire la même opération avec des threads : écrivez un programme composé de 4 nou-
	veaux threads. Chaque thread incrémente un million de fois une variable globale compteur de type long
	int. Une fois les threads terminés, affichez la valeur du compteur. Vous réaliserez directement la solution
	correcte en vous aidant d’une synchronisation à base de mutex.

	pthread_mutex lock unlock

	Comparez les deux versions (IPC System V et threads).

	Tout dabord, sans atomicité, on peut remarquer que le resultat obtenus avec les threads est different. Il faut faire beaucoup plus d'operation
	qu'avec les fork pour avoir des operations qui se croisent. De plus, avec les barriere, la solution thread est beaucoup plus rapide.
*/


#include <stdio.h> // print
#include <stdlib.h> // exit
#include <pthread.h>

#define NB_THREAD 	4

long int COMPTEUR;


int main(int argc, char **argv){

	int errno;
	pthread_t activites[NB_THREAD];
	pthread_mutex_t mutex_activites = PTHREAD_MUTEX_INITIALIZER;

	void * incrementer(void *arg){

		
		int i;
		for(i=0; i<1000000; i++){
			pthread_mutex_lock(& mutex_activites);			
			(COMPTEUR)++;
			pthread_mutex_unlock(& mutex_activites);		
		}
		
		pthread_exit(arg);

	}

	int i;


	for(i=0; i<NB_THREAD; i++){

		if((errno = pthread_create(&activites[i], NULL, incrementer, NULL)) != 0){
			perror("Pthread create");
			exit(EXIT_FAILURE);
		};

	}

	for(i=0; i<NB_THREAD; i++){

		if((errno = pthread_join(activites[i], NULL)) != 0){
			perror("Pthread join");
		};

	}


	printf("Valeur finale %ld\n", COMPTEUR);
	
	
	

	
	exit(0);
}
