/**
 * Author: Kevin Masson
 * L3 Informatique 2016 Automne
 * SC TP Noté
 */
  
/**
 * 
 * La liste simplement chainée ne pouvant être réalisée à l'aide de pointeurs
 * 
 * Il n'est pas possible d'utiliser des pointeurs en mémoire partagée car 
 * les addresses des pointeurs ne poiteront pas vers la même donnée puisque 
 * les pointeurs ne référencents pas la mémoire physique
 * mais la mémoire propre au processus
 * 
 */
   
#ifndef H_LIBMAGASIN
#define H_LIBMAGASIN

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>

struct article{
	int reference;
	int quantite_sem_id;
};

struct segment{
	int taille;
	int id_sem;
	int id_shm;
	struct article articles[0];
};

key_t key_magasin(){
	return ftok("/etc/passwd", 72);
}

key_t key_article(int ref){
	return ftok("/etc/passwd", 72+ref);
}

struct segment* creer_segment(int taille){
	// Creation du segment
	int shmid;

	shmid = shmget(key_magasin(), sizeof(struct segment) + sizeof(struct article) * taille, IPC_CREAT|IPC_EXCL|0600);
	
	if(shmid==-1){
		perror("Cannot create segment\n");
		exit(EXIT_FAILURE);
	}	
	
	struct segment* stock = (struct segment*) shmat(shmid, 0, 0);
	
	stock->id_shm = shmid;
	stock->taille = taille;		
	
	// Creation du semaphore pour ajout une reference
	int semid;
	
	semid = semget(key_magasin(), 1, IPC_CREAT|0600);
	if(semid==-1){
		perror("Cannot create semaphore\n");
		exit(EXIT_FAILURE);
	}	
	semctl(semid, 0, SETVAL, 1);
	
	stock->id_sem = semid;
	
	// Init les articles
	
	int i;
	for(i = 0; i < taille; i++){
		stock->articles[i].reference = -1;
	}
	
	return stock;
}

struct segment* recuperer_segment(){
	int shmid;
	shmid = shmget(key_magasin(), 0, 0);
	if(shmid==-1){
		perror("Cannot get segment\n");
		exit(EXIT_FAILURE);
	}		
	struct segment* seg = (struct segment*) shmat(shmid, 0, 0);
	return seg;
}

int place_libre(struct segment* seg){
	int i;
	int libre = 0;
	for(i = 0; i < seg->taille; i++){
		if(seg->articles[i].reference == -1) ++libre;
	}
	return libre;
}


int ref_existe(struct segment* seg, int ref){
		int i;
		for(i = 0; i < seg->taille; i++){
			if(seg->articles[i].reference == ref){
				return 1;
			}
		}
		return -1;
}

int ajouter_ref(struct segment* seg, int ref){
	int i;
	
	struct sembuf op;
	op.sem_num = 0;
	op.sem_flg = 0;
	
	op.sem_op = -1; // LOCK
	semop(seg->id_sem, &op, 1);	
	
	if(ref_existe(seg, ref)==1){
		op.sem_op = +1; // UNLOCK
		semop(seg->id_sem, &op, 1);	
		return -1;
	}
	
	for(i = 0; i < seg->taille; i++){
		if(seg->articles[i].reference == -1){
			seg->articles[i].reference = ref;
			
			// Creation du semaphore pour ajout une reference
			int semid;
			
			semid = semget(key_article(ref), 1, IPC_CREAT|0600);
			if(semid==-1){
				perror("Cannot create semaphore\n");
				exit(EXIT_FAILURE);
			}	
			semctl(semid, 0, SETVAL, 0);
			
			seg->articles[i].quantite_sem_id = semid;			
			break;
		}
	}
	
	op.sem_op = +1; // UNLOCK
	semop(seg->id_sem, &op, 1);	
	
	
	return 0;
}

int ajouter_qte(struct segment* seg, int ref, int qt){
	int i;
	
	struct sembuf op;
	op.sem_num = 0;
	op.sem_flg = 0;
	
	op.sem_op = -1; // LOCK
	semop(seg->id_sem, &op, 1);	
	
	if(ref_existe(seg, ref)==-1){
		op.sem_op = +1; // UNLOCK
		semop(seg->id_sem, &op, 1);	
		return -1;
	}
	
	for(i = 0; i < seg->taille; i++){
		if(seg->articles[i].reference == ref){

			
			// Creation du semaphore pour ajout une reference
			int qtdefault = semctl(seg->articles[i].quantite_sem_id, 0, GETVAL, 0);
			if(qtdefault == -1){
				op.sem_op = +1; // UNLOCK
				semop(seg->id_sem, &op, 1);	
				return -5;
			}
			if(semctl(seg->articles[i].quantite_sem_id, 0, SETVAL, qtdefault + qt)==-1){
				op.sem_op = +1; // UNLOCK
				semop(seg->id_sem, &op, 1);	
				return -6;
			}
			break;
		}
	}
	
	op.sem_op = +1; // UNLOCK
	semop(seg->id_sem, &op, 1);	
	
	
	return 0;
}

#endif
