/**
 * Author: Kevin Masson
 * L3 Informatique 2016 Automne
 * SC TP Noté
 */
 
#include "libmagasin.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <signal.h>



int main(int argc, const char * argv[]){
	
	if( argc < 2 ){
			printf("USAGE: %s ref [ref ...]\n", argv[0]);
	}
	
	struct segment* seg = recuperer_segment();
	
	int libre = place_libre(seg);
	
	printf("Il rest %d place dans le stock\n", libre);
	
	if(libre < argc-1){
		printf("Il n'y a pas assez de place pour %d nouvelle(s) ref.\n", argc-1);
		exit(EXIT_FAILURE);
	}
	
	int i;
	int ajout = 0;
	for(i = 1; i<argc; i++){
		int fb = ajouter_ref(seg, atoi(argv[i]));
		if(fb == -1){
				printf("La reference %d existe deja\n", atoi(argv[i]));
		}else{
			++ajout;
		}
	}
	
	printf("%d ref ont étés ajotuées\n", ajout);
	
	//printf("semid is %d and shm id is %d\n", seg->id_sem, seg->id_shm);
	
	
	
	
	return 0;
}
