/**
 * Author: Kevin Masson
 * L3 Informatique 2016 Automne
 * SC TP Noté
 */
 
#include "libmagasin.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <signal.h>



int main(int argc, const char * argv[]){
	
	if( argc != 2 ){
			printf("USAGE: %s nref\n", argv[0]);
	}
	
	int nsegment = atoi(argv[1]);
	
	struct segment* seg = creer_segment(nsegment);
	printf("Segement créer avec l'id %d\n", seg->id_shm); // Utiliser pour pouvoir recuperer l'id et le supprimer a la main		
	
	// Creation du signal de fermeture du magasin
	
	sigset_t emptymask;
	if(sigemptyset(&emptymask) <0){
		perror("Sigemptyset");
		exit(EXIT_FAILURE);
	}
	
	void close(int signo){
		
	}

	struct sigaction sigclose;
	sigclose.sa_handler = &close;
	sigclose.sa_mask = emptymask;
	sigclose.sa_flags = 0;
	
	if(sigaction(SIGINT, &sigclose, NULL) < 0){
		perror("Sigint sigaction");
		exit(EXIT_FAILURE);
	}
	
	
	printf("Magasin ouvert\n");
	
	pause();
	
	printf("Magasin fermé\n");
	
	
	
	semctl(seg->id_sem, 0, IPC_RMID, 1);
	shmctl(seg->id_shm, IPC_RMID, 0);
	
	
	return 0;
}
