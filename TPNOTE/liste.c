/**
 * Author: Kevin Masson
 * L3 Informatique 2016 Automne
 * SC TP Noté
 */
 
#include "libmagasin.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <signal.h>



int main(int argc, const char * argv[]){	
	
	struct segment* seg = recuperer_segment();	
	
	struct sembuf op;
	op.sem_num = 0;
	op.sem_flg = 0;
	
	op.sem_op = -1; // LOCK
	semop(seg->id_sem, &op, 1);	
	
	printf("Magasin, taille(%d), place(%d)\n", seg->taille, place_libre(seg));
	
	int i;
	for(i = 0; i < seg->taille; i++){
		if(seg->articles[i].reference != -1){
			int qtdefault = semctl(seg->articles[i].quantite_sem_id, 0, GETVAL, 0);
			printf("Ref %d, quantite(%d)\n", seg->articles[i].reference, qtdefault);
		}
	}

	op.sem_op = +1; // UNLOCK
	semop(seg->id_sem, &op, 1);		
	
	
	
	
	return 0;
}
