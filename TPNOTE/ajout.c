/**
 * Author: Kevin Masson
 * L3 Informatique 2016 Automne
 * SC TP Noté
 */
 
#include "libmagasin.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <signal.h>



int main(int argc, const char * argv[]){
	
	if( argc < 3 && (argc-1)%2 != 0){
			printf("USAGE: %s ref qte [ref qte...]\n", argv[0]);
	}
	
	struct segment* seg = recuperer_segment();	
	
	int i;
	for(i = 1; i<argc; i+=2){
		int ref = atoi(argv[i]);
		int qt = atoi(argv[i+1]);
		int fb = ajouter_qte(seg, ref, qt);
		if(fb == -1){
			printf("La reference %d n'existe pas\n", ref);
		}else if(fb == 0){
			printf("%d ont etait ajoutés à %d\n", qt, ref);
		}else{
			printf("Impossible de modifier la quantite de %d\n", ref);
		}
	}
		
	//printf("semid is %d and shm id is %d\n", seg->id_sem, seg->id_shm);
	
	
	
	
	return 0;
}
