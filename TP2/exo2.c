/**
 * TP 2 - SC - L3 Info - 09/11/16
 * Kevin Masson
 * Exercice 2
 */

 
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>


#define FICHIER "/etc/passwd"
#define PROJET 01654

struct segment{
	int n;
	int ic; int ip;
	int data[];
};

int creer_shm(int n){
	int errno;
	key_t key_shm = ftok(FICHIER, PROJET);

	int ex = shmget(key_shm, 0, 0);

	if(ex == -1){
		if(errno != ENOENT){
			printf("shm existant");
			exit(EXIT_FAILURE); 
		}
	}else{
		if(shmctl(ex, IPC_RMID, NULL) == -1){
			printf("shm suppression impossible");
			exit(EXIT_FAILURE); 
		}
	}

	int id = shmget(key_shm, sizeof(struct segment) + n * sizeof(int) , IPC_CREAT|0660);
	if(id == -1){
		printf("creation impossible");
		exit(EXIT_FAILURE); 	
	}
	
	return id;
}

void supprimer_shm(int shmid){
	if(shmctl(shmid, IPC_RMID, NULL) == -1){
		printf("shm suppression impossible");
		exit(EXIT_FAILURE); 
	}
}

int lire_entier(struct segment *seg){
	if(seg->ic == seg->ip){
		return -1;
	}else{
		int r = seg->data[seg->ic];
		seg->ic = (seg->ic + 1) % seg->n;
		return r;
	}
}

int main(int argc, char **argv){

	struct segment *seg;
	
	int id = creer_shm(5);
	void * addr;
	addr = shmat(id, NULL, NULL);
	seg = (struct segment*) addr;
	seg->n=5;


		

}
