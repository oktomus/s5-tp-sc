/**
 * TP 2 - SC - L3 Info - 09/11/16
 * Kevin Masson
 * Exercice 1
 * 
 * Creation de N threads
 * Reception SIGINT -> kill un thread parmis n
 * pthread_sigmask
 * 
 * algo:
 * 	sigaction (2x): reponse a un signal
 * 	create
 * 
 *  ---v
 * 	|pause
 * 	|kill
 * 	|join
 * 	--<
 * 
 * 		sigmask
 * 		pause
 * 	
 * Deffinission des signaux a partir du processus et pas du thread
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

void close_and_wait_thread(){

	printf("Closing a thread\n");	

}

void kill_thread(){
	printf("Killing myself\n");
}

void * fonction_thread(void *arg){
	
	sigmask(SIGINT);

	pthread_barrier_wait(arg);

	pause();

	pthread_exit(arg);

}

int main(int argc, char **argv){

	int errno;

	if(argc != 2){
		printf("USAGE: exo1 number_threads");
	}		

	// Reception signal arret

	sigset_t emptymask;
	if(sigemptyset(&emptymask) <0){
		perror("Sigemptyset");
		exit(EXIT_FAILURE);
	}

	struct sigaction sigclose;
	sigclose.sa_handler = &close_and_wait_thread;
	sigclose.sa_mask = emptymask;
	sigclose.sa_flags = 0;

	

	if(sigaction(SIGINT, &sigclose, NULL) < 0){
		perror("Sigint sigaction");
		exit(EXIT_FAILURE);
	}

	// Reception signal 

	struct sigaction sigthread;
	sigthread.sa_handler = &kill_thread;
	sigthread.sa_mask = emptymask;
	sigthread.sa_flags = 0;

	if(sigaction(SIGUSR1, &sigthread, NULL) < 0){
		perror("Sigusr1 sigaction");
		exit(EXIT_FAILURE);
	}

	// Creation des threads

	int nb_thread = atoi(argv[1]);

	printf("Creation de %d threads\n", nb_thread);

	pthread_t activites[nb_thread];
	
	// Barriere
	pthread_barrier_t barrier;
	pthread_barrier_init(&barrier, NULL, nb_thread+1);


	int i;


	for(i=0; i<nb_thread; i++){

		if((errno = pthread_create(&activites[i], NULL, fonction_thread, &barrier)) != 0){
			perror("Pthread create");
			exit(EXIT_FAILURE);
		};

	}

	pthread_barrier_wait(&barrier);

	for(i=0; i<nb_thread; i++){

		pause();
		// Tuer un thread pthread_kill
		pthread_kill(activites[i], SIGUSR1);
		if((errno = pthread_join(activites[i], NULL)) != 0){
			perror("Pthread join");
		};
	

	}

	exit(0);
	

	
	
}
